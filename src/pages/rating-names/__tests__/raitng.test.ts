import { test, expect } from '@playwright/test';
import { errorResponse } from '../__mocks__/error-response';
import { RatingPage, ratingPageFixture } from '../__page-object__/rating-page-object'

const ratingTest = test.extend<{ ratingPage: RatingPage }>({
    ratingPage: ratingPageFixture,
});

ratingTest('При ошибке сервера в методе raiting - отображается попап ошибки', async ({ page, ratingPage  }) => {
    ratingPage.replaceResponse(errorResponse);
    ratingPage.openRatingPage();
    await test.step(`Проверяю что отображается текст при ошибке сервера`, async () => {
        await expect(page.getByText('Ошибка загрузки рейтинга')).toBeVisible();
    })
});

ratingTest('Рейтинг котиков отображается', async ({ ratingPage }) => {
    await ratingPage.openRatingPage();
    const likesElements = await ratingPage.getPositiveRatingElements();
    const likesValues: number[] = [];
    for (const element of likesElements) {
        const text: string = await element.textContent();
        const number: number = parseInt(text);
        likesValues.push(number);
    }
    const sortedLikesValues : number[] = [...likesValues].sort((a, b) => b - a);

    await test.step(`Проверяю что рейтинг формируется по убыванию`, async () => {
       await expect(likesValues).toEqual(sortedLikesValues);
    })
});