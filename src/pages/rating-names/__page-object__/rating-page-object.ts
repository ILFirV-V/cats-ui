import type { Page, TestFixture } from '@playwright/test';
import { test } from '@playwright/test';
import { errorResponseType } from '../__mocks__/error-response';

export class RatingPage {
    private page: Page;
    private ratingPositiveSelector: string
    private ratingNegativeSelector: string
    constructor({
        page,
    }: {
        page: Page;
    }) {
        this.page = page;
        this.ratingPositiveSelector = '.rating-names_item-count__1LGDH.has-text-success';
        this.ratingNegativeSelector = '.rating-names_item-count__1LGDH.has-text-danger';
    }

    async openRatingPage() {
        return await test.step('Открываю страницу рейтинга приложения', async () => {
            await this.page.goto('/rating')
        })
    }

    async getPositiveRatingElements() {
        return await test.step(`Получаю элементы из рейтинга`, async () => {
            //return await this.page.$$('//*[@class="rating-names_item-count__1LGDH has-text-success"]');
            return await this.page.$$(this.ratingPositiveSelector);
        })
    }

    async getNegativeRatingElements() {
        return await test.step(`Получаю элементы из рейтинга`, async () => {
            return await this.page.$$(this.ratingNegativeSelector);
        })
    }

    async replaceResponse(errorResponse: errorResponseType) {
        return await test.step(`Подменяю ответ при ошибке сервера`, async () => {
            await this.page.route('/api/likes/cats/rating', route => {
                route.fulfill(errorResponse);
            });
        })
    }
}

export type RatingPageFixture = TestFixture<
    RatingPage,
    {
        page: Page;
    }
>;

export const ratingPageFixture: RatingPageFixture = async (
    { page },
    use
) => {
    const ratingPage = new RatingPage({ page });

    await use(ratingPage);
};

