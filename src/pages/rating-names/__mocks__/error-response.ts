export const errorResponse : errorResponseType = {
    status: 500,
    contentType: 'application/json',
    body: JSON.stringify({ error: 'Internal Server Error' }),
};

export type errorResponseType = {
    status: number,
    contentType: string,
    body: string,
}